package com.atlantis.springtemplate.controllers;


import com.atlantis.entities.models.*;
import com.atlantis.entities.services.*;
import com.atlantis.springtemplate.EmitLogDirect;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private DeviceTypeService deviceTypeService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private UserService userService;

    @Autowired
    private MetricService metricService;

    @PostMapping("/api/mobile/devices")
    public Device devices(@RequestBody Map<String, Object> body) {
        DeviceType deviceType = deviceTypeService.getOne(UUID.fromString((String) body.get("device_type")));
        Group group = groupService.getOne(UUID.fromString((String) body.get("group")));
        Device device = new Device();
        device.setName((String) body.get("name"));
        device.setDevice_type(deviceType);
        return deviceService.save(device);
    }

    @GetMapping("/api/mobile/groups")
    public List<Group> listGroups(@RequestParam(name="userId", defaultValue = "") UUID userId) {
        return userService.getOne(userId).getGroups();
    }

    @GetMapping("/api/mobile/devices/{id}") // TODO: Validate that this device is linked to the user
    public Device one(@PathVariable("id") UUID id) {
        return deviceService.getOne(id);
    }

    @GetMapping("/api/mobile/devices")
    public List<Device> all(HttpServletRequest request) {
        List<Device> devices = new ArrayList<>();
        String uid = request.getAttribute("userOidFromToken").toString();
        userService.getOneByToken(uid)
                .getGroups()
                .parallelStream()
                .forEach(deviceGroup -> devices.addAll(deviceGroup.getDevices()));
        return devices;
    }

    @PostMapping("/api/mobile/devices/{id}/cmd") // TODO: Validate that this device is linked to the user
    public Boolean deviceCmd(@PathVariable("id") UUID id, @RequestParam(name="command", defaultValue = "") String command) {
        EmitLogDirect emitLogDirect= new EmitLogDirect();
        JsonObject instruction = new JsonObject();
        instruction.addProperty("id", id.toString());
        instruction.addProperty("command", command);
        try {
            emitLogDirect.emit(instruction.toString(), "broker.atlantis.com","device_command");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @GetMapping("/api/mobile/devices/{id}/metrics") // TODO: Validate that this device is linked to the user
    public List<Metric> deviceMetrics(@PathVariable("id") UUID id, @RequestParam(name="page", defaultValue = "1") String page) {
        int pageNum = Integer.parseInt(page);
        Pageable pageWithElements = PageRequest.of(pageNum, 20);
        Device device = deviceService.getOne(id);
        return metricService.findByDevicePaginated(device, pageWithElements);
    }
}
