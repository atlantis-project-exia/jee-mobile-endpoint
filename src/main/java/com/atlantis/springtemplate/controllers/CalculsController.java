package com.atlantis.springtemplate.controllers;


import com.atlantis.springtemplate.models.dto.CalculsDataDto;
import com.atlantis.springtemplate.models.dto.CalculsDatumDto;
import com.atlantis.springtemplate.models.dto.CalculsTypesDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CalculsController {

    @Value("${calculsApi.url}")
    private String calculsApiUrl;

    private final RestTemplate restTemplate;

    public CalculsController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/api/mobile/calculs/{starting}/{ending}/{deviceId}")
    public Object[] listTypes(@PathVariable String starting, @PathVariable String ending, @PathVariable String deviceId) {
        String url = calculsApiUrl + "/" + starting + "/" + ending + "/" + deviceId;
        System.out.println(url);
        ResponseEntity<Object[]> responseEntity = restTemplate.getForEntity(url, Object[].class);
        Object[] objects = responseEntity.getBody();
        MediaType contentType = responseEntity.getHeaders().getContentType();
        HttpStatus statusCode = responseEntity.getStatusCode();
        return objects;
//        CalculsDataDto calculsObject = restTemplate.getForObject(url, CalculsDataDto.class);
//        assert calculsObject != null;
//        return calculsObject.getDatas();
//        List<CalculsDatumDto> test = new ArrayList<>();
//        return test;
    }
}
