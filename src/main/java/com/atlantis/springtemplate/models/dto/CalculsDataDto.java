package com.atlantis.springtemplate.models.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class CalculsDataDto {
    public List<CalculsDatumDto> datas;

    public CalculsDataDto() {
        datas = new ArrayList<>();
    }
}
