package com.atlantis.springtemplate.models.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class CalculsDatumDto {
    public UUID id;
    public UUID computed_id_computed;
    public float value_computed;
    public String type_computed;
    public String created_at_computed;
    public UUID device_id_device;
}
