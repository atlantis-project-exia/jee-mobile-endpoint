package com.atlantis.springtemplate.models.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class CalculsTypesDto {
    public ArrayList<String> types;
}
