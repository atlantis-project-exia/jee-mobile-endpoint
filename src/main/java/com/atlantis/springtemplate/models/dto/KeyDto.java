package com.atlantis.springtemplate.models.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class KeyDto {
    private String kid;
    private int nbf;
    private String use;
    private String kty;
    private String e;
    private String n;
}