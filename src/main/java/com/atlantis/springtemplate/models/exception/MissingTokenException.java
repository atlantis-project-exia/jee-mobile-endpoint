package com.atlantis.springtemplate.models.exception;

public class MissingTokenException extends Exception {
    public MissingTokenException(String message) {
        super(message);
    }
}
