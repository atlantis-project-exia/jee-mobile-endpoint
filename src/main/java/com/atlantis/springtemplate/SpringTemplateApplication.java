package com.atlantis.springtemplate;

import com.atlantis.entities.services.UserService;
import com.atlantis.springtemplate.interceptor.TokenInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication(scanBasePackages = {
        "com.atlantis.springtemplate.controllers",
        "com.atlantis.entities.repositories",
        "com.atlantis.entities.services"
})
@EntityScan("com.atlantis.entities.models")
@EnableJpaRepositories("com.atlantis.entities.repositories")
public class SpringTemplateApplication implements WebMvcConfigurer {
    @Autowired
    private UserService userService;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Override
    public void addInterceptors (InterceptorRegistry registry) {
        registry.addInterceptor(new TokenInterceptor(userService));
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringTemplateApplication.class, args);
    }

}
