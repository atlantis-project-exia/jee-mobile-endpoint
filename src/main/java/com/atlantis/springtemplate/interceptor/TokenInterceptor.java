package com.atlantis.springtemplate.interceptor;

import com.atlantis.entities.models.User;
import com.atlantis.entities.services.UserService;
import com.atlantis.springtemplate.models.dto.KeyDto;
import com.atlantis.springtemplate.models.dto.KeysDto;
import com.atlantis.springtemplate.models.exception.*;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;
import java.util.Map;
import java.util.UUID;

public class TokenInterceptor implements HandlerInterceptor {

    private static final String RSA = "RSA";
    private static final String MICROSOFT_KEYS_URL = "https://atlantisproject.b2clogin.com/atlantisproject.onmicrosoft.com/discovery/v2.0/keys?p=b2c_1_signuporsignin";

    private UserService userService;
    public TokenInterceptor(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        KeysDto keys;
        Map<String, String[]> requestParameters = request.getParameterMap();

        if (requestParameters == null || requestParameters.isEmpty() || !requestParameters.containsKey("token")) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            throw new MissingTokenException("The token is missing");
        }

        String token = requestParameters.get("token")[0];

        try {
            keys = restTemplate.getForObject(MICROSOFT_KEYS_URL, KeysDto.class);
        } catch (HttpClientErrorException e) {
            response.setStatus(HttpStatus.SERVICE_UNAVAILABLE.value());
            throw new CannotReadKeyException();
        }

        if (keys == null) {
            response.setStatus(HttpStatus.SERVICE_UNAVAILABLE.value());
            throw new CannotReadKeyException();
        }

        try {
            DecodedJWT jwt = JWT.decode(token);
            String kid = jwt.getKeyId();
            KeyDto goodKey = keys.getKeys().parallelStream()
                    .filter(key -> kid.equals(key.getKid()))
                    .findFirst()
                    .orElse(null);
            BigInteger modulus = new BigInteger(1, Base64.getUrlDecoder().decode(goodKey.getN())); // TODO: get key corresponding to "kid"
            BigInteger exponent = new BigInteger(1, Base64.getUrlDecoder().decode(goodKey.getE()));
            KeyFactory factory = KeyFactory.getInstance(RSA);
            RSAPublicKeySpec spec = new RSAPublicKeySpec(modulus, exponent);
            RSAPublicKey publicKey = (RSAPublicKey) factory.generatePublic(spec);

            RSAPrivateKeySpec privateSpec = new RSAPrivateKeySpec(modulus, exponent);
            RSAPrivateKey privateKey = (RSAPrivateKey) factory.generatePrivate(privateSpec);

            Algorithm algorithm = Algorithm.RSA256(publicKey, privateKey);
            JWTVerifier verifier = JWT.require(algorithm).build();
            verifier.verify(token);

            try {
                String oid = jwt.getClaims().get("oid").asString();
                String firstName = jwt.getClaims().get("given_name").asString();
                String lastName = jwt.getClaims().get("family_name").asString();

                User user = userService.getOneByToken(oid);
                if (user == null) user = new User();
                user.setTokens(oid);
                user.setFirst_name(firstName);
                user.setLast_name(lastName);
                userService.createOrUpdateDirect(user);

                request.setAttribute("userOidFromToken", oid);

            } catch (JWTDecodeException e){
                System.out.println(e);
            }

            return true;
        } catch (InvalidKeySpecException e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            throw new CannotGenerateKeysException();
        } catch (NoSuchAlgorithmException e) {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            throw new AlgorithmDoesnotExistException();
        } catch (com.auth0.jwt.exceptions.TokenExpiredException e) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            throw new TokenExpiredException();
        } catch (JWTVerificationException e) {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            throw new CannotCheckTokenSignatureException();
        }
    }
}
